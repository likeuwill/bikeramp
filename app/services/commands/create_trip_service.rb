module Commands
  class CreateTripService < Mutations::Command
    API_URL = 'https://maps.googleapis.com/maps/api/distancematrix/json?origins='.freeze
    API_KEY = ENV['GOOGLE_MAPS_API_KEY']

    # These inputs are required
    required do
      string :start_address
      string :destination_address
      string :price
      string :date
    end

    # The execute method is called only if the inputs validate.
    def execute
      if distance
        Api::Trip.create!(trip_params.merge(distance: distance))
      else
        add_error(:address, :invalid, 'Invalid start_address or destination_address')
      end
    end

    private

    def distance
      api_response['rows'].first['elements'].first.dig('distance', 'value')
    end

    def api_response
      @api_response ||= log_errors { api_call.parsed_response }
    end

    def api_call
      HTTParty.get(URI.escape(url))
    end

    def url
      "#{API_URL}#{start_address}&destinations=#{destination_address}&key=#{API_KEY}"
    end

    def log_errors
      yield
    rescue StandardError => e
      Rails.logger.error e.message
      Rails.logger.error e.backtrace.join("\n")
    end

    def trip_params
      inputs.slice(:start_address, :destination_address, :price, :date)
    end
  end
end
