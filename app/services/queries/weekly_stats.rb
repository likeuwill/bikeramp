module Queries
  class WeeklyStats < ::Interfaces::Query
    # retrieves stats from current week
    def execute
      fetch_weekly_stats
    end

    private

    def fetch_weekly_stats
      ActiveRecord::Base.connection.execute(query).first
    end

    def query
      <<~SQL
        SELECT
          CAST(COALESCE(SUM(distance / 1000),0) as INT) as total_distance,
          CAST(COALESCE(SUM(price),0) as FLOAT)as total_price
        FROM trips;
      SQL
    end
  end
end
