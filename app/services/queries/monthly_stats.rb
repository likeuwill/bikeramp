module Queries
  class MonthlyStats < ::Interfaces::Query
    # retrieves stats from current month, grouped by day
    def execute
      fetch_monthly_stats
    end

    private

    def fetch_monthly_stats
      ActiveRecord::Base.connection.execute(query).to_a
    end

    def query
      <<~SQL
        SELECT
          date as day,
          CAST(SUM(distance / 1000) as INT) as total_distance,
          CAST(AVG(distance / 1000) as INT) as avg_ride,
          CAST(AVG(price) as FLOAT) as avg_price
        FROM trips
        WHERE abstime(date) >  CURRENT_DATE - INTERVAL '1 months'
        GROUP BY day
        ORDER BY day;
      SQL
    end
  end
end
