module Api
  class Trip < ApplicationRecord
    include ActiveModel::Serialization

    validates :start_address, :destination_address, :price, :distance, :date, presence: true
  end
end
