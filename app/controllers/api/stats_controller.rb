module Api
  class StatsController < ApplicationController
    # GET /stats/weekly
    def weekly
      weekly_stats = ::Queries::WeeklyStats.run

      render json: weekly_stats, serializer: WeeklyStatsSerializer
    end

    # GET /stats/monthly
    def monthly
      monthly_stats = ::Queries::MonthlyStats.run

      render json: monthly_stats, each_serializer: MonthlyStatsSerializer
    end
  end
end
