module Api
  class TripsController < ApplicationController
    # POST /trips
    def create
      outcome = ::Commands::CreateTripService.run(trip_params)

      if outcome.success?
        render json: outcome.result, status: :created, location: outcome.result
      else
        render json: outcome.errors.symbolic, status: :unprocessable_entity
      end
    end

    private

    def trip_params
      params.require(:trip).permit(:start_address, :destination_address, :price, :date)
    end
  end
end
