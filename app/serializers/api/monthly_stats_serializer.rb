module Api
  class MonthlyStatsSerializer < StatsSerializer
    attributes :day, :avg_ride, :avg_price

    def day
      # Change date format to e.g.("April, 4th")
      Time.zone.at(object['day']).to_formatted_s(:month_and_day)
    end

    def avg_ride
      "#{object['avg_ride']}km"
    end

    def avg_price
      "#{object['avg_price']}PLN"
    end
  end
end
