module Api
  class StatsSerializer < ActiveModel::Serializer
    attributes :total_distance

    def total_distance
      "#{object['total_distance']}km"
    end
  end
end
