module Api
  class WeeklyStatsSerializer < StatsSerializer
    attributes :total_price

    def total_price
      "#{object['total_price']}PLN"
    end
  end
end
