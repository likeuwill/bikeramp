require 'rails_helper'

describe Api::TripsController, type: :request do
  let(:trip_params) { attributes_for(:trip) }
  let(:params) { { trip: trip_params } }

  # TO DO: Mock response from google api
  describe 'POST #create' do
    subject { post api_trips_path, params: params }

    before do
      allow_any_instance_of(Commands::CreateTripService).to receive(:distance).and_return(3833)
    end

    context 'with valid params' do
      it 'creates a new Trip' do
        expect { subject }.to change(Api::Trip, :count).by(1)
      end

      it 'contains correct keys' do
        subject
        expect(response_json.keys).to contain_exactly('id', 'start_address', 'destination_address',
                                                      'price', 'date', 'distance')
      end

      it 'renders a JSON response with the new trip' do
        subject
        expect(response).to have_http_status(:created)
        expect(response.location).to eq(api_trip_url(Api::Trip.last))
      end
    end

    context 'with invalid params' do
      before do
        allow_any_instance_of(Commands::CreateTripService).to receive(:distance).and_return(nil)
      end

      describe 'with missing required params' do
        let(:params) { { trip: trip_params.slice!(:start_address) } }

        it 'responds with errors' do
          subject
          expect(response).to have_http_status(:unprocessable_entity)
          expect(response_json['start_address']).to eq 'required'
        end
      end

      describe 'with invalid address' do
        let(:params) { { trip: attributes_for(:trip, destination_address: 'Invalid address') } }

        it 'responds with errors' do
          subject
          expect(response).to have_http_status(:unprocessable_entity)
          expect(response_json['address']).to eq 'invalid'
        end
      end
    end
  end
end
