require 'rails_helper'

describe Api::StatsController, type: :request do
  let!(:trip) { create_list(:trip, 3) }

  describe 'GET #weekly' do
    subject { get weekly_api_stats_path }

    it 'contains correct keys' do
      subject
      expect(response_json.keys).to contain_exactly('total_distance', 'total_price')
    end

    it 'contains correct values' do
      subject
      expect(response_json['total_distance']).to match(/km/)
      expect(response_json['total_price']).to match(/PLN/)
    end

    context 'no trips for current week' do
      before do
        Api::Trip.delete_all
      end

      it 'returns zero values' do
        subject
        expect(response_json.keys).to contain_exactly('total_distance', 'total_price')
        expect(response_json.values).to contain_exactly('0km', '0.0PLN')
      end
    end
  end

  describe 'GET #monthly' do
    let!(:yesterday_trip) { create(:trip, :yesterday_trip) }

    subject { get monthly_api_stats_path }

    it 'contains correct keys' do
      subject
      expect(response_json.first.keys)
        .to contain_exactly('day', 'total_distance', 'avg_ride', 'avg_price')
    end

    it 'contains correct values' do
      subject
      current_month_and_day = 1.day.ago.to_formatted_s(:month_and_day)

      expect(response_json.first['day']).to match(current_month_and_day)
      expect(response_json.first['total_distance']).to match(/km/)
      expect(response_json.first['avg_ride']).to match(/km/)
      expect(response_json.first['avg_price']).to match(/PLN/)
    end

    context 'no trips for current month' do
      before do
        Api::Trip.delete_all
      end

      it 'returns empty array' do
        subject
        expect(response_json).to match_array([])
      end
    end
  end
end
