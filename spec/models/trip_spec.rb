require 'rails_helper'

describe Api::Trip, type: :model do
  it { should validate_presence_of(:start_address) }
  it { should validate_presence_of(:destination_address) }
  it { should validate_presence_of(:price) }
  it { should validate_presence_of(:date) }

  context 'validate author attributes' do
    let(:trip) { build(:trip) }

    it 'is valid' do
      expect(trip).to be_valid
    end
  end
end
