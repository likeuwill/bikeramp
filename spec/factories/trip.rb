FactoryBot.define do
  factory :trip, class: Api::Trip do
    start_address { 'Plac Europejski 2, Warszawa, Polska' }
    destination_address { 'Aleja Prymasa Tysiąclecia 103, Warszawa, Polska' }
    price { 32.06 }
    distance { 3833 }
    date { Time.now.utc.to_i }
    created_at { Time.now.utc }
    updated_at { created_at }
  end

  trait :yesterday_trip do
    price { 22.16 }
    distance { 2040 }
    date { 1.day.ago.utc.to_i }
    created_at { Time.now.utc }
    updated_at { created_at }
  end
end
