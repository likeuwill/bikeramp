require 'rails_helper'

describe Api::TripsController, type: :routing do
  describe 'routing' do
    it 'routes to #create' do
      expect(post: '/api/trips').to route_to('api/trips#create')
    end
  end
end
