Rails.application.routes.draw do
  root 'api/stats#monthly'

  namespace :api do
    resources :trips

    resources :stats, only: [] do
      get :weekly, on: :collection
      get :monthly, on: :collection
    end
  end
end
