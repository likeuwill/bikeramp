Time::DATE_FORMATS[:month_and_day] = lambda { |date|
  day_format = ActiveSupport::Inflector.ordinalize(date.day)
  date.strftime("%B, #{day_format}") # => "April, 25th"
}
