# Project Description
Imagine you are a bike courier and you want to build a system that will help you track your rides during delivery of packages: how many kilometers did you ride on each day and how much did customer pay for delivery. The app will help you to control your work. The codename for the app is: `Bikeramp` :)

` Remember! No login required. Write just the API, without UI!`

## Trips

#### Create Trip
This endpoint logs the trip and automatically calculates the distance between start and destination addresses.

###### HTTP Request
`POST http://localhost:3000/api/trips`

* Query Parameters
### `Parameter` `Description`
##### `start_address` `Start address in format: "Plac Europejski 2, Warszawa, Polska`
##### `destination_address` `Aleja Prymasa Tysiąclecia 103, Warszawa, Polska`
##### `price` `Package price in PLN`
##### `date` `Date of delivery`

## Stats

#### Get Weekly Stats
This endpoint retrieves how many kilometers did courier rode during current week and how much money he received on the rides.

###### HTTP Request
`GET http://localhost:3000/api/stats/weekly`

```json
{
  "total_distance": "40km",
  "total_price":    "49.75PLN"
}
```
curl `http://localhost:3000//api/stats/weekly`

#### Get Monthly Stats
This endpoint retrieves summary of ride distances from current month, grouped by day. The summary should include sum of all rides distances from given day, average ride distance and average price for the ride.

###### HTTP Request
`GET http://localhost:3000/api/stats/monthly`

```json
[
  {
    "day":            "July, 4th",
    "total_distance": "12km",
    "avg_ride":       "4km",
    "avg_price":      "22.75PLN"
  },
  {
    "day":            "July, 5th",
    "total_distance": "3km",
    "avg_ride":       "3km",
    "avg_price":      "15.5PLN"
  }
]
```
curl `http://localhost:3000//api/stats/monthly`

## System dependencies
* Ruby on Rails: v5.2.3
* Ruby: v2.6.1
* PostgreSQL: v11.2
